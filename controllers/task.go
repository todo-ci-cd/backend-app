package controllers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/mtyuksel/Todo-TDD/backend/Helpers"
	"github.com/mtyuksel/Todo-TDD/backend/models"
	"github.com/mtyuksel/Todo-TDD/backend/models/Interfaces"
)

type TaskController struct {
	repository Interfaces.IRepository
}

func NewTaskController(repository Interfaces.IRepository) *TaskController {
	return &TaskController{repository: repository}
}

func (controller TaskController) returnResponseBySuccess(ctx *fiber.Ctx, data interface{}, success bool) {
	if success {
		successResponse := Helpers.GenerateResponse(true, "", data)
		ctx.JSON(successResponse)

		return
	}

	errResponse := Helpers.GenerateResponse(false, "An error occurred.", nil)

	ctx.Status(fiber.StatusBadRequest)
	ctx.JSON(errResponse)
}

func (controller TaskController) Add(ctx *fiber.Ctx) error {
	task := models.Task{}

	ctx.BodyParser(&task)
	addedTask, err := controller.repository.Insert(task)

	if err != nil {
		controller.returnResponseBySuccess(ctx, nil, false)
		return err
	}

	ctx.Status(fiber.StatusCreated)
	controller.returnResponseBySuccess(ctx, models.TaskDTO{Task: *addedTask}, true)

	return nil
}

func (controller TaskController) GetAll(ctx *fiber.Ctx) error {
	tasks, err := controller.repository.GetAll()

	if err != nil {
		controller.returnResponseBySuccess(ctx, nil, false)
		return err
	}

	ctx.Status(fiber.StatusOK)
	controller.returnResponseBySuccess(ctx, tasks, true)

	return nil
}
