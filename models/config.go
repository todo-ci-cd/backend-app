package models

type ConfigModel struct {
	Environment string
	MongoUrl string
}
