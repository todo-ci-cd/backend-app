package Interfaces

import "github.com/mtyuksel/Todo-TDD/backend/models"

type IRepository interface {
	Insert(task models.Task) (*models.Task, error)
	GetAll() (*models.TaskList, error)
}