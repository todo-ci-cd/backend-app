package unit

import (
	"github.com/mtyuksel/Todo-TDD/backend/Helpers"
	"github.com/mtyuksel/Todo-TDD/backend/models"
	"testing"
)

func TestGetConfigByEnvForProd(t *testing.T) {
	expectedValue := models.ConfigModel{
		Environment: "prod",
		MongoUrl: "mongodb://localhost:2717",
	}

	result := Helpers.GetConfigByEnv("prod")

	if *result != expectedValue {
		t.Error("Failed to fetch config for prod environment.")
	}
}

func TestGetConfigByEnvForDev (t *testing.T) {
	expectedValue := models.ConfigModel{
		Environment: "dev",
		MongoUrl: "mongodb://localhost:2718",
	}

	result := Helpers.GetConfigByEnv("dev")

	if *result != expectedValue {
		t.Error("Failed to fetch config for dev environment.")
	}
}