package unit

import (
	"github.com/mtyuksel/Todo-TDD/backend/Helpers"
	"testing"
)

func TestCheckIfTextContainsStars(t *testing.T) {
	value := "****My t*eSt VA**lue**"
	expectedValue := true

	result := Helpers.CheckIfTextContainsStars(value)

	if result != expectedValue {
		t.Fail()
	}
}

func TestRemoveStars(t *testing.T) {
	value := "****My t*est va**lue**"
	expectedValue := "My test value"

	result := Helpers.RemoveStars(value)

	if result != expectedValue {
		t.Error("Expected '" + value + "' to converted '" + expectedValue + "'")
	}
}