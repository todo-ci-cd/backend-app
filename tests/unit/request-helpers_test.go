package unit

import (
	"testing"

	"github.com/mtyuksel/Todo-TDD/backend/Helpers"
	"github.com/mtyuksel/Todo-TDD/backend/models"
)

func TestGenerateResponseFor_ErrorResult(t *testing.T) {
	expectedValue := models.Response{
		Success: false,
		Message: "its test error",
		Data:    nil,
	}

	result := Helpers.GenerateResponse(false, "its test error", nil)

	if *result != expectedValue {
		t.Error("Failed to generate response for error result.")
	}
}

func TestGenerateResponseFor_SuccessResult(t *testing.T) {
	expectedTask := models.Task{
		Id:   "123123",
		Name: "test task",
	}

	expectedValue := models.Response{
		Success: true,
		Message: "",
		Data: expectedTask,
	}

	result := Helpers.GenerateResponse(true, "", expectedTask)

	if *result != expectedValue {
		t.Error("Failed to generate response for success result.")
	}
}
