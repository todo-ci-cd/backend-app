package mocks

import (
	"github.com/mtyuksel/Todo-TDD/backend/models"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

type taskRepositoryMock struct {
	client *mongo.Client
}

func NewTaskRepositoryMock(url string) *taskRepositoryMock {
	return &taskRepositoryMock{}
}

func (t taskRepositoryMock) Insert(task models.Task) (*models.Task, error) {
	task.Id = bson.NewObjectId()
	return &task, nil
}

func (t taskRepositoryMock) GetAll() (*models.TaskList, error) {
	tasks := []models.Task{
		models.Task{
			Id: "1",
			Name: "Buy some milk",
		},
		models.Task{
			Id: "2",
			Name: "Visit parents",
		},
		models.Task{
			Id: "3",
			Name: "Do homewoks",
		},
	}

	taskList := models.TaskList{Tasks: tasks }

	return &taskList, nil
}