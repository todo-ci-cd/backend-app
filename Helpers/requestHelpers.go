package Helpers

import "github.com/mtyuksel/Todo-TDD/backend/models"

func GenerateResponse(success bool, message string, data interface{}) *models.Response {
	return &models.Response{
		Success: success,
		Message: message,
		Data:    data,
	}
}
