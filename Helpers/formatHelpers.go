package Helpers

import "strings"

func CheckIfTextContainsStars(text string) bool {
	return strings.Contains(text, "*")
}

func RemoveStars(text string) string {
	return strings.Replace(text, "*", "", -1)
}