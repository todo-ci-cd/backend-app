package Helpers

import "github.com/mtyuksel/Todo-TDD/backend/models"

func GetConfigByEnv(env string) *models.ConfigModel {
	config := &models.ConfigModel{}

	switch env {
	case "prod": config = getProd()
	case "dev": config = getDev()
	}

	return config
}

func getProd() *models.ConfigModel{
	config := models.ConfigModel{
		Environment: "prod",
		MongoUrl: "mongodb://localhost:2717",
	}
	return &config
}

func getDev() *models.ConfigModel{
	config := models.ConfigModel{
		Environment: "dev",
		MongoUrl: "mongodb://localhost:2718",
	}
	return &config
}
