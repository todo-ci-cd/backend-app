module github.com/mtyuksel/Todo-TDD/backend

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.21.0
	github.com/pact-foundation/pact-go v1.6.4
	go.mongodb.org/mongo-driver v1.7.4
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
