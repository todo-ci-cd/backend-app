package main

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/mtyuksel/Todo-TDD/backend/Helpers"
	"github.com/mtyuksel/Todo-TDD/backend/controllers"
	"github.com/mtyuksel/Todo-TDD/backend/repositories"
)

func main() {
	LaunchServer()
}

func LaunchServer() {
	env := os.Getenv("env")

	config := Helpers.GetConfigByEnv(env)

	repository := repositories.NewTaskRepository(config.MongoUrl)
	taskController := controllers.NewTaskController(repository)

	app := fiber.New()

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST",
	}))

	app.Post("/tasks/add", taskController.Add)
	app.Get("/tasks/getall", taskController.GetAll)

	app.Listen(":9000")
}
