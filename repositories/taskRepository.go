package repositories

import (
	"context"
	"github.com/mtyuksel/Todo-TDD/backend/models"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type taskRepository struct {
	client *mongo.Client
}

func NewTaskRepository(url string) *taskRepository {
	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)
	clientOptions := options.Client().ApplyURI(url)
	client, _ := mongo.Connect(ctx, clientOptions)
	return &taskRepository{client}
}

func (t taskRepository) Insert(task models.Task) (*models.Task, error) {
	task.Id = bson.NewObjectId()

	collection :=  t.client.Database("todo").Collection("tasks")
	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)
	_, err := collection.InsertOne(ctx, task)

	if err != nil {
		return nil, err
	}

	return &task, nil
}

func (t taskRepository) GetAll() (*models.TaskList, error) {
	collection := t.client.Database("todo").Collection("tasks")
	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})

	defer cursor.Close(ctx)
	taskList := models.TaskList{}
	for cursor.Next(ctx) {
		task := models.Task{}
		cursor.Decode(&task)
		if len(task.Name) > 0 {
			taskList.Tasks = append(taskList.Tasks, task)
		}
	}

	if err = cursor.Err(); err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	return &taskList, nil
}